﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{

    private Camera camera;
    private GameProperties gameProperties;
    private Transform playerTransform;
    private Rigidbody2D body;

    private Vector3 tempPos;

    private void Start()
    {
        camera = GetComponent<Camera>();
        body = GetComponent<Rigidbody2D>();
        gameProperties = GameManager.GameProperties;
        playerTransform = FindObjectOfType<PlayerController>().transform;
    }

    private void LateUpdate()
    {
        tempPos = playerTransform.position;
        tempPos.z = -10;
        tempPos += gameProperties.CameraOffset;
        if (!Mathf.Approximately(camera.orthographicSize, gameProperties.CameraDistance))
            camera.orthographicSize = gameProperties.CameraDistance;

    }

    private void FixedUpdate()
    {
        if (gameProperties.FollowPlayer)
        {
            body.MovePosition(Vector3.Lerp(tempPos, transform.position, gameProperties.CameraDelay));
        }
    }
}
