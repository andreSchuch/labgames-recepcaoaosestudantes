﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Game Properties", menuName = "Game Properties")]
public class GameProperties : ScriptableObject
{
    [Header("Movement")]
    public float WalkSpeed = 8f;
    public int MaxJumps = 2;
    public float JumpStrength = 12;
    public float SpringJumpMultiplier = 1.2f;
    public float Gravity = -9.8f;

    [Header("Camera")]
    public bool FollowPlayer = false;
    public float CameraDistance = 5;
    public Vector3 CameraOffset;
    public float CameraDelay;

    [Header("Walking Enemy")]
    public float WalkingEnemyPatrolSpeed = 3f;
    public float WalkingEnemyRunSpeed = 4.5f;
    public float WalkingEnemyPathRange = 2.5f;
    public float WalkEnemySightRadius = 5f;

    [Header("Turret Enemy")]
    public float TurretSightRadius = 5f;
    public float TurretFireRate = 1f;
    public float TurretShotSpeed = 5f;
}
