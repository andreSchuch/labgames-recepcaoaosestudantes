﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public Transform Feet;

    private GameProperties gameProperties;
    private Animator animator;
    private Rigidbody2D body;

    private Vector3 rightDirection;
    private Vector3 leftDirection;
    private int jumpsAvailable;
    private bool isJumping = false;
    private bool canCheckGrounded = true;

    private int lifes = 0;
    private bool isAlive = true;

    private RaycastHit2D hit;

    private void Start()
    {
        lifes = 2;

        gameProperties = GameManager.GameProperties;
        animator = GetComponent<Animator>();
        body = GetComponent<Rigidbody2D>();

        jumpsAvailable = gameProperties.MaxJumps;

        rightDirection = new Vector3(0f, 0f, 0f);
        leftDirection = new Vector3(0f, 180f, 0f);
    }

    private void Update()
    {
        if (isAlive)
        {
            CheckGrounded();
            Jump();
            Move();
        }
    }

    private void CheckGrounded()
    {
        hit = Physics2D.Linecast(Feet.position, (Vector2)Feet.position + Vector2.down * 0.1f);
        if (!isJumping)
        {
            if (hit.collider != null && hit.collider.GetComponent<MovingPlatform>() != null)
                transform.parent = hit.transform;
            else
                transform.parent = null;
        }
        else if (isJumping && canCheckGrounded)
        {
            if (hit.collider != null)
            {
                jumpsAvailable = gameProperties.MaxJumps;
                // Reached the ground
                isJumping = false;
                animator.SetTrigger("Ground");
            } else
                transform.parent = null;
        }

    }

    private void Move()
    {
        if (!Mathf.Approximately(Physics2D.gravity.y, gameProperties.Gravity))
            Physics2D.gravity = new Vector2(0f, gameProperties.Gravity);

        var horInput = Input.GetAxis("Horizontal");
        if (horInput > .2f)
            transform.eulerAngles = rightDirection;
        else if (horInput < -.2)
            transform.eulerAngles = leftDirection;
        //float verInput = Input.GetAxis("Vertical");
        animator.SetFloat("Speed", Mathf.Abs(horInput));

        body.velocity = new Vector2(horInput * gameProperties.WalkSpeed, body.velocity.y);
    }

    private void Jump()
    {
        if (Input.GetKeyDown(KeyCode.Space) && jumpsAvailable > 0)
        {
            jumpsAvailable--;
            ApplyJumpForce();
        }
    }

    private void ApplyJumpForce(float multiplier = 1f)
    {
        isJumping = true;
        canCheckGrounded = false;
        body.velocity = new Vector2(body.velocity.x, 0f);
        body.AddForce(Vector2.up * gameProperties.JumpStrength * multiplier, ForceMode2D.Impulse);
        animator.SetTrigger("Jump");
        Invoke("DisableGroundedCheck", 0.3f);
    }

    private void TakeDamage(int damage = 1)
    {
        lifes -= damage;
        if (lifes <= 0)
        {
            isAlive = false;
            animator.SetTrigger("Die");
            Invoke("ResetGame", 2f);
            body.isKinematic = true;
            body.velocity = Vector2.zero;
            body.angularVelocity = 0f;
        }
        else
        {
            animator.SetTrigger("TakeDamage");
        }
    }

    void DisableGroundedCheck()
    {
        canCheckGrounded = true;
    }

    void ResetGame()
    {
        GameManager.ResetGame();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (!isAlive) return;
        if (other.CompareTag("Enemy"))
        {
            if (body.velocity.y < -1)
            {
                body.velocity = new Vector2(body.velocity.x, 0f);
                body.AddForce(Vector2.up * gameProperties.JumpStrength, ForceMode2D.Impulse);
                Destroy(other.gameObject);
            }
            else
            {
                TakeDamage();
            }
        }

        if (other.CompareTag("Bullet"))
        {
            TakeDamage();
            Destroy(other.gameObject);
        }

        if (other.CompareTag("Spike"))
        {
            TakeDamage(lifes);
        }

        if (other.CompareTag("Spring") && body.velocity.y < -.1)
            ApplyJumpForce(gameProperties.SpringJumpMultiplier);
    }
}
