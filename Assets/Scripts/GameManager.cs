﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{

    private static GameProperties _gameProperties;
    public static GameProperties GameProperties
    {
        get
        {
            if (_gameProperties != null)
                return _gameProperties;

            _gameProperties = Resources.Load("Game Properties") as GameProperties;
            if (_gameProperties != null)
                return _gameProperties;

            Debug.LogError("Lacking Game Properties File");
            return null;
        }
    }

    private GUIStyle guiStyle;

    private bool showMovementParameters = false;
    private bool showCameraParameters = false;
    private bool showWalkingEnemyParameters = false;
    private bool showTurretParameters = false;
    
    private void Awake()
    {
        if (GameProperties) {}
        guiStyle = new GUIStyle{normal = {textColor = Color.black}};
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.F1))
        {
            showMovementParameters = !showMovementParameters;
            if (showMovementParameters)
            {
                showCameraParameters = false;
                showTurretParameters = false;
                showWalkingEnemyParameters = false;
            }
        }
        if (Input.GetKeyDown(KeyCode.F2))
        {
            showCameraParameters = !showCameraParameters;
            if (showCameraParameters)
            {
                showMovementParameters = false;
                showTurretParameters = false;
                showWalkingEnemyParameters = false;
            }
        }

        if (Input.GetKeyDown(KeyCode.F3))
        {
            showWalkingEnemyParameters = !showWalkingEnemyParameters;
            if (showWalkingEnemyParameters)
            {
                showMovementParameters = false;
                showTurretParameters = false;
                showCameraParameters = false;
            }
        }

        if (Input.GetKeyDown(KeyCode.F4))
        {
            showTurretParameters = !showTurretParameters;
            if (showTurretParameters)
            {
                showMovementParameters = false;
                showCameraParameters = false;
                showWalkingEnemyParameters = false;
            }
        }
        if (Input.GetKeyDown(KeyCode.F5))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }

    public static void ResetGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    private void OnDisable()
    {
      //  Undo.RecordObject(GameProperties, "Game properties changed");
    }

    private void OnGUI()
    {
        var y = 20f;
        GUI.Label(new Rect(20, y, 128, 32), "[F1] - Movimento", guiStyle);

        y += 20;
        GUI.Label(new Rect(20, y, 128, 32), "[F2] - Camera", guiStyle);

        y += 20;
        GUI.Label(new Rect(20, y, 128, 32), "[F3] - Inimigo Andarilho", guiStyle);

        y += 20;
        GUI.Label(new Rect(20, y, 128, 32), "[F4] - Inimigo Montado", guiStyle);

        y += 20;
        GUI.Label(new Rect(20, y, 128, 32), "[F5] - Reiniciar", guiStyle);

        if (showMovementParameters)
        {
            y += 30;
            GUI.Label(new Rect(20, y, 128, 32), "-------- Movimento -------", guiStyle);

            y += 30;
            GUI.Label(new Rect(20, y, 128, 32), "Velocidade de caminhada: " + GameProperties.WalkSpeed, guiStyle);
            y += 20;
            
            GameProperties.WalkSpeed = GUI.HorizontalSlider(new Rect(20, y, 100, 32), GameProperties.WalkSpeed, 0f, 20f);
            RoundToXDecimals(ref GameProperties.WalkSpeed, 2);

            y += 30;
            GUI.Label(new Rect(20, y, 128, 32), "Máximo de pulos: " + GameProperties.MaxJumps, guiStyle);
            y += 20;
            GameProperties.MaxJumps = (int)GUI.HorizontalSlider(new Rect(20, y, 100, 32), GameProperties.MaxJumps, 0, 5);

            y += 30;
            GUI.Label(new Rect(20, y, 128, 32), "Força do pulo: " + GameProperties.JumpStrength, guiStyle);
            y += 20;
            GameProperties.JumpStrength = GUI.HorizontalSlider(new Rect(20, y, 100, 32), GameProperties.JumpStrength, 0f, 30f);
            RoundToXDecimals(ref GameProperties.JumpStrength, 2);

            y += 30;
            GUI.Label(new Rect(20, y, 128, 32), "Multiplicador da mola: " + GameProperties.SpringJumpMultiplier, guiStyle);
            y += 20;
            GameProperties.SpringJumpMultiplier = GUI.HorizontalSlider(new Rect(20, y, 100, 32), GameProperties.SpringJumpMultiplier, 0.1f, 3f);
            RoundToXDecimals(ref GameProperties.SpringJumpMultiplier, 2);

            y += 30;
            GUI.Label(new Rect(20, y, 128, 32), "Força da gravidade: " + GameProperties.Gravity, guiStyle);
            y += 20;
            GameProperties.Gravity = GUI.HorizontalSlider(new Rect(20, y, 100, 32), GameProperties.Gravity, 0f, -50f);
            RoundToXDecimals(ref GameProperties.Gravity, 2);
        }
        if (showCameraParameters)
        {
            y += 30;
            GUI.Label(new Rect(20, y, 128, 32), "-------- Camera -------", guiStyle);

            y += 30;
            GUI.Label(new Rect(20, y, 128, 32), "Seguir jogador: ", guiStyle);
            GameProperties.FollowPlayer = GUI.Toggle(new Rect(110,y - 2, 128, 32), GameProperties.FollowPlayer, "");

            y += 30;
            GUI.Label(new Rect(20, y, 128, 32), "Distância: " + GameProperties.CameraDistance, guiStyle);
            y += 20;
            GameProperties.CameraDistance = GUI.HorizontalSlider(new Rect(20, y, 100, 32), GameProperties.CameraDistance, 1f, 15f);
            RoundToXDecimals(ref GameProperties.CameraDistance, 2);

            y += 30;
            GUI.Label(new Rect(20, y, 128, 32), "Offset de posição: ", guiStyle);
            y += 20;
            GUI.Label(new Rect(20, y, 15, 32), "X: ", guiStyle);
            GameProperties.CameraOffset.x = GUI.HorizontalSlider(new Rect(40,y, 50, 32), GameProperties.CameraOffset.x, -4f, 4f);
            GUI.Label(new Rect(100, y, 15, 32), "Y: ", guiStyle);
            GameProperties.CameraOffset.y = GUI.HorizontalSlider(new Rect(120,y, 50, 32), GameProperties.CameraOffset.y, -4f, 4f);

            y += 30;
            GUI.Label(new Rect(20, y, 128, 32), "Tempo de atraso: " + GameProperties.CameraDelay, guiStyle);
            y += 20;
            GameProperties.CameraDelay = GUI.HorizontalSlider(new Rect(20, y, 100, 32), GameProperties.CameraDelay, 0f, 1f);
            RoundToXDecimals(ref GameProperties.CameraDelay, 2);
        }

        if (showWalkingEnemyParameters)
        {
            y += 30;
            GUI.Label(new Rect(20, y, 128, 32), "-------- Inimigo Andarilho -------", guiStyle);

            y += 30;
            GUI.Label(new Rect(20, y, 128, 32), "Velocidade de patrulha: " + GameProperties.WalkingEnemyPatrolSpeed, guiStyle);
            y += 20;
            GameProperties.WalkingEnemyPatrolSpeed = GUI.HorizontalSlider(new Rect(20, y, 100, 32), GameProperties.WalkingEnemyPatrolSpeed, 0.5f, 8f);
            RoundToXDecimals(ref GameProperties.WalkingEnemyPatrolSpeed, 2);

            y += 30;
            GUI.Label(new Rect(20, y, 128, 32), "Velocidade de perseguição: " + GameProperties.WalkingEnemyRunSpeed, guiStyle);
            y += 20;
            GameProperties.WalkingEnemyRunSpeed = GUI.HorizontalSlider(new Rect(20, y, 100, 32), GameProperties.WalkingEnemyRunSpeed, 0.5f, 12f);
            RoundToXDecimals(ref GameProperties.WalkingEnemyRunSpeed, 2);

            y += 30;
            GUI.Label(new Rect(20, y, 128, 32), "Alcance do caminho: " + GameProperties.WalkingEnemyPathRange, guiStyle);
            y += 20;
            GameProperties.WalkingEnemyPathRange = GUI.HorizontalSlider(new Rect(20, y, 100, 32), GameProperties.WalkingEnemyPathRange, .5f, 5f);
            RoundToXDecimals(ref GameProperties.WalkingEnemyPathRange, 2);

            y += 30;
            GUI.Label(new Rect(20, y, 128, 32), "Alcance da visão: " + GameProperties.WalkEnemySightRadius, guiStyle);
            y += 20;
            GameProperties.WalkEnemySightRadius = GUI.HorizontalSlider(new Rect(20, y, 100, 32), GameProperties.WalkEnemySightRadius, .25f, 8f);
            RoundToXDecimals(ref GameProperties.WalkEnemySightRadius, 2);
        }

        if (showTurretParameters)
        {
            y += 30;
            GUI.Label(new Rect(20, y, 128, 32), "-------- Inimigo Montado -------", guiStyle);

            y += 30;
            GUI.Label(new Rect(20, y, 128, 32), "Raio de visão: " + GameProperties.TurretSightRadius, guiStyle);
            y += 20;
            GameProperties.TurretSightRadius = GUI.HorizontalSlider(new Rect(20, y, 100, 32), GameProperties.TurretSightRadius, 0.1f, 12f);
            RoundToXDecimals(ref GameProperties.TurretSightRadius, 2);

            y += 30;
            GUI.Label(new Rect(20, y, 128, 32), "Tempo entre disparos: " + GameProperties.TurretFireRate, guiStyle);
            y += 20;
            GameProperties.TurretFireRate = GUI.HorizontalSlider(new Rect(20, y, 100, 32), GameProperties.TurretFireRate, 0.1f, 3f);
            RoundToXDecimals(ref GameProperties.TurretFireRate, 2);

            y += 30;
            GUI.Label(new Rect(20, y, 128, 32), "Velocidade do projétil: " + GameProperties.TurretShotSpeed, guiStyle);
            y += 20;
            GameProperties.TurretShotSpeed = GUI.HorizontalSlider(new Rect(20, y, 100, 32), GameProperties.TurretShotSpeed, 0.25f, 12f);
            RoundToXDecimals(ref GameProperties.TurretShotSpeed, 2);
        }
    }

    private void RoundToXDecimals(ref float number, int decimals)
    {
        var factor = Mathf.Pow(10, decimals);
        number = (Mathf.Round(number * factor) / factor);
    }
}
