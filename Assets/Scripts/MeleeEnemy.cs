﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeEnemy : MonoBehaviour
{

    private GameProperties gameProperties;
    private Vector3 startingPoint;

    private float movementFactor = 0f;
    private float offset;
    private float delta;
    private bool isPatrolling = true;

    private Transform playerTransform;
    private Vector3 targetPosition;

    private void Start()
    {
        InitializeProperties();
        playerTransform = FindObjectOfType<PlayerController>().transform;
    }

    private void Update()
    {
        if (isPatrolling)
        {
            FollowPath();
            CheckPlayerProximity();
        }
        else
            FollowPlayer();
        
    }

    private void CheckPlayerProximity()
    {
        if (!(Vector3.Distance(transform.position, playerTransform.position) <
              gameProperties.WalkEnemySightRadius)) return;
        isPatrolling = false;
        //GetComponent<SpriteRenderer>().color = Color.red;
    }


    private void FollowPlayer()
    {
        targetPosition = playerTransform.position;
        targetPosition.y = transform.position.y;

        transform.eulerAngles = targetPosition.x < transform.position.x ? new Vector3(0f,180f,0f) : new Vector3(0f,0f,0f);

        transform.position = Vector3.MoveTowards(transform.position, targetPosition, gameProperties.WalkingEnemyRunSpeed * Time.deltaTime);
    }

    private void FollowPath()
    {
        movementFactor += Time.deltaTime * gameProperties.WalkingEnemyPatrolSpeed;
        if (movementFactor > 1000)
            movementFactor = 0f;

        delta = offset;
        offset = Mathf.Sin(movementFactor) * gameProperties.WalkingEnemyPathRange * 0.5f;
        delta = offset - delta;
        transform.position = startingPoint + Vector3.right * offset;

        transform.eulerAngles = delta > 0 ? new Vector3(0f, 0f, 0f) : new Vector3(0f, 180f, 0f);
    }

    private void InitializeProperties()
    {
        gameProperties = GameManager.GameProperties;
        startingPoint = transform.position;
    }

    private void OnDrawGizmos()
    {
        if (!Application.isPlaying)
        {
            InitializeProperties();
        }

        Gizmos.color = Color.green;
        Gizmos.DrawLine(startingPoint - Vector3.right * gameProperties.WalkingEnemyPathRange * 0.5f, startingPoint + Vector3.right * gameProperties.WalkingEnemyPathRange * 0.5f);

        //Gizmos.color = Color.yellow;
        //Gizmos.DrawWireSphere(transform.position, gameProperties.WalkEnemySightRadius);
    }
}
