﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour
{
    public float Speed = 1.5f;
    public float Range = 3f;
    public float Delay = 0f;
    public bool Vertical = false;

    private Vector3 startingPosition;
    private float moveFactor;
    private float delayCounter = 0f;
    private Vector3 maxDelta;

    private void Start()
    {
        startingPosition = transform.position;
    }

    private void FixedUpdate()
    {
        if (delayCounter < Delay)
        {
            delayCounter += Time.fixedDeltaTime;
            return;
        }
        Move();
    }

    private void Move()
    {
        UpdateMaxDelta();
        moveFactor += Time.fixedDeltaTime * Speed;
        transform.position = startingPosition + Mathf.Sin(moveFactor) * maxDelta;
    }

    private void UpdateMaxDelta()
    {
        maxDelta = (Vertical ? Vector3.up : Vector3.right) * Range / 2;
    }

    private void OnDrawGizmos()
    {
        if(!Application.isPlaying)
            startingPosition = transform.position;
        UpdateMaxDelta();
        Gizmos.color = Color.red;
        Gizmos.DrawLine( startingPosition + - maxDelta, startingPosition + maxDelta );
    }
}
