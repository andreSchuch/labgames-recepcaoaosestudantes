﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShooterEnemy : MonoBehaviour {
	public float visionRange = 3f;
	public float shotCadency = 1f;
	public float shotVelocity = 5f;

	public Transform ShotOrigin;
	public GameObject ShotPrefab;

    private GameProperties gameProperties;
    private Transform player;
    private SpriteRenderer spriteRenderer;
    private bool isShooting = false;

	void Start ()
	{
		player = FindObjectOfType<PlayerController>().transform;
		gameProperties = GameManager.GameProperties;
	    spriteRenderer = GetComponent<SpriteRenderer>();
		StartCoroutine(ShootRoutine());
	}

	void Update () {
		if (Vector2.Distance(player.position, transform.position) < gameProperties.TurretSightRadius)
		{
		    if (!isShooting)
		    {
		        spriteRenderer.color = Color.red;
		        isShooting = true;
		    }

			if (transform.position.x > player.position.x) {
				transform.eulerAngles = new Vector3 (0f, 180f, 0f);
			} else {
				transform.eulerAngles = new Vector3 (0f, 0f, 0f);
			}
		}
		else if (isShooting)
		{
		    spriteRenderer.color = new Color(0.52f, 0.79f, 1f);
		    isShooting = false;
		}
	}

	private void OnDrawGizmos()
	{
	    if (!Application.isPlaying)
	    {
	        gameProperties = GameManager.GameProperties;
	    }
		if (gameProperties == null) return;
		Gizmos.color = Color.yellow;
		Gizmos.DrawWireSphere(transform.position, gameProperties.TurretSightRadius);
	}

	IEnumerator ShootRoutine()
	{
		while (true)
		{
			if (isShooting)
			{
				Shoot(transform.right);
			}
			yield return new WaitForSeconds(gameProperties.TurretFireRate);
		}
	}

	private void Shoot(Vector2 direction)
	{
		var shot = Instantiate(ShotPrefab, ShotOrigin.position, Quaternion.identity);
		shot.GetComponent<Rigidbody2D>().velocity = direction * gameProperties.TurretShotSpeed;
	}
}
