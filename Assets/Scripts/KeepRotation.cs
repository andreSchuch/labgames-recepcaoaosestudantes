﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeepRotation : MonoBehaviour
{
    private Vector3 startingRotation;

	void Start ()
	{
	    startingRotation = transform.eulerAngles;
	}
	
	void LateUpdate ()
	{
	    transform.eulerAngles = startingRotation;
	}
}
