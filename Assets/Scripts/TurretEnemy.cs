﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretEnemy : MonoBehaviour
{

    public float visionRange = 3f;
    public float shotCadency = 1f;
    public float shotVelocity = 5f;

    public GameObject shotPrefab;

    private GameProperties gameProperties;
    private Transform player;
    public bool isShooting = false;

	void Start ()
	{
	    player = FindObjectOfType<PlayerController>().transform;
	    gameProperties = GameManager.GameProperties;
	    StartCoroutine(ShootRoutine());
	}
	
	void Update () {
        if (Vector2.Distance(player.position, transform.position) < gameProperties.TurretSightRadius)
	    {
	        isShooting = true;
	    }
	    else
	    {
	        isShooting = false;
	    }
	}

    void OnDrawGizmos()
    {
        if (gameProperties == null) return;
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, gameProperties.TurretSightRadius);
    }

    IEnumerator ShootRoutine()
    {
        while (true)
        {
            if (isShooting)
            {
                Shoot(Vector2.left);
                Shoot(Vector2.right);
            }
            yield return new WaitForSeconds(gameProperties.TurretFireRate);
        }
    }

    private void Shoot(Vector2 direction)
    {
        var shot = Instantiate(shotPrefab, transform.position, Quaternion.identity);
        shot.GetComponent<Rigidbody2D>().velocity = direction * gameProperties.TurretShotSpeed;
    }
}
